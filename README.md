# Arvore-geradora-minima
**Trabalho de AEDS**

Você é o responsável por configurar os roteadores do Coltec. Os roteadores transmitem os
dados entre si através dos cabos de internet. Os dados transmitidos podem trafegar por
uma ou mais rotas para serem entregues ao destinatário.
O preço dos cabos de rede utilizados nos roteadores do Coltec pode chegar a ser muito
caro, e com o último corte do governo, precisamos economizar.
Você deve modificar a infra­estrutura da rede do Coltec de forma com que todos os
roteadores consigam transmitir dados entre si e exista somente uma rota entre cada par de
roteadores, economizando o máximo possível de cabos de internet.
Implemente um programa para resolver esse seu problema!
